# Safe Creative
Minecraft plugin which restricts the create of custom items to creative mode.

## Installation

Put jar to the plugins directory on your bukkit server

## Permissions

- **safe-creative.bypass** - bypass restricts the creation of custom items in creative mode
- **safe-creative.group.building_blocks** - allow to create standard gamemode creative items from build blocks group
- **safe-creative.group.decorations** - allow to create standard gamemode creative items from decorations group
- **safe-creative.group.misc** - allow to create standard gamemode creative items from misc group
- **safe-creative.group.food** - allow to create standard gamemode creative items from food group
- **safe-creative.group.transportation** - allow to create standard gamemode creative items from transportation group
- **safe-creative.group.redstone** - allow to create standard gamemode creative items from redstone group
- **safe-creative.group.combat** - allow to create standard gamemode creative items from combat group
- **safe-creative.group.brewing** - allow to create standard gamemode creative items from brewing group
- **safe-creative.group.tools** - allow to create standard gamemode creative items from tools group
