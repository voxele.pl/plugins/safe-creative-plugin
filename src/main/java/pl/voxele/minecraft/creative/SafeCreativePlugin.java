package pl.voxele.minecraft.creative;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sk89q.worldedit.WorldEdit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import pl.voxele.minecraft.creative.worldedit.AntiWorldEditCrash;

import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.logging.Level;

public class SafeCreativePlugin
        extends JavaPlugin
{
    private JsonObject items;

    @Override
    public void onEnable()
    {
        String version = this.getBukkitVersion();
        if (version == null)
        {
            this.getLogger().severe("Could not to detect server version");
            this.getPluginLoader().disablePlugin(this);
            return;
        }
        this.getLogger().info("Detected server version '" + version + "'");

        JsonObject items;
        try (Reader reader = this.getTextResource("items/items_" + version + ".json"))
        {
            if (reader == null)
            {
                this.getLogger().severe("This version of server is not supported, found version '" + version + "'");
                this.getPluginLoader().disablePlugin(this);
                return;
            }
            JsonElement element = new JsonParser().parse(reader);
            if (!element.isJsonObject())
            {
                this.getLogger().severe("Invalid safe creative items format");
                this.getPluginLoader().disablePlugin(this);
                return;
            }
            items = element.getAsJsonObject();
        }
        catch (IOException e)
        {
            this.getLogger().log(Level.SEVERE, "Could not parse safe creative items", e);
            this.getPluginLoader().disablePlugin(this);
            return;
        }
        ReflectionHelper reflectionHelper;
        try
        {
            reflectionHelper = new ReflectionHelper(this.getNmsVersion());
        }
        catch (ClassNotFoundException | NoSuchFieldException | NoSuchMethodException e)
        {
            this.getLogger().log(Level.SEVERE, "This version of server is not supported", e);
            this.getPluginLoader().disablePlugin(this);
            return;
        }
        CreateUnsafeItems createUnsafeItems = new CreateUnsafeItems(this.getLogger(), reflectionHelper, items);

        PluginManager pluginManager = this.getServer().getPluginManager();
        pluginManager.registerEvents(createUnsafeItems, this);

        boolean isWorldEditEnabled = pluginManager.isPluginEnabled("WorldEdit");
        boolean isFastAsyncWorldEditEnabled = pluginManager.isPluginEnabled("FastAsyncWorldEdit");
        this.getLogger().info("WorldEdit: " + isWorldEditEnabled);
        this.getLogger().info("FastAsyncWorldEdit: " + isFastAsyncWorldEditEnabled);

        if (isWorldEditEnabled || isFastAsyncWorldEditEnabled)
        {
            WorldEdit.getInstance().getEventBus().register(new AntiWorldEditCrash());
        }

        this.getLogger().info("Plugins was successfully enabled");
    }

    public String getBukkitVersion()
    {
        String version = this.getNmsVersion();
        if (version == null)
        {
            return null;
        }
        return String.join("_", Arrays.copyOf(version.split("_"), 2));
    }

    public String getNmsVersion()
    {
        String[] strings = this.getServer().getClass().getCanonicalName().split("\\.");
        if (strings.length < 4)
        {
            return null;
        }
        return strings[3];
    }
}
