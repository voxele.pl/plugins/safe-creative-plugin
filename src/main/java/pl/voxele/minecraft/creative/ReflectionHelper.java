package pl.voxele.minecraft.creative;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.internal.LazilyParsedNumber;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Set;
import java.util.function.Predicate;

public class ReflectionHelper
{
    // org.bukkit.craftbukkit.inventory.CraftItemStack
    private final Method bukkitItemAsNmsCopyMethod;

    // net.minecraft.server.ItemStack
    private final Method saveItemMethod;

    // net.minecraft.server.NBTBase
    private final Class<?> nbtBaseClass;

    // net.minecraft.server.NBTNumber
    private final Class<?> nbtNumberClass;
    private final Method nbtNumberAsNumberMethod;

    // net.minecraft.server.NBTTagString
    private final Class<?> nbtStringClass;
    private final Method nbtStringAsStringMethod;

    // net.minecraft.server.NBTTagCompound
    private final Class<?> nbtCompoundClass;
    private final Constructor<?> nbtCompoundConstructor;
    private final Method nbtCompoundRemoveMethod;
    private final Method nbtCompoundGetKeysMethod;
    private final Method nbtCompoundGetMethod;

    public ReflectionHelper(String nmsVersion)
            throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException
    {
        this.nbtBaseClass = findClass("net.minecraft.nbt.NBTBase", "net.minecraft.server." + nmsVersion + ".NBTBase");

        this.nbtCompoundClass = findClass("net.minecraft.nbt.NBTTagCompound",
                "net.minecraft.server." + nmsVersion + ".NBTTagCompound");
        this.nbtCompoundConstructor = this.nbtCompoundClass.getConstructor();

        String removeMethodName;
        if (nmsVersion.startsWith("v1_18"))
        {
            removeMethodName = "r";
        }
        else
        {
            removeMethodName = "remove";
        }
        this.nbtCompoundRemoveMethod = this.nbtCompoundClass.getMethod(removeMethodName, String.class);
        this.nbtCompoundGetKeysMethod = getMethodByReturnType(this.nbtCompoundClass, Set.class, null);
        this.nbtCompoundGetMethod = getMethodByReturnType(this.nbtCompoundClass, this.nbtBaseClass, null, String.class);

        Class<?> nmsItemClass = findClass("net.minecraft.world.item.ItemStack",
                "net.minecraft.server." + nmsVersion + ".ItemStack");
        this.saveItemMethod = getMethodByReturnType(nmsItemClass, this.nbtCompoundClass, null, this.nbtCompoundClass);

        Class<?> bukkitItemClass = Class.forName("org.bukkit.craftbukkit." + nmsVersion + ".inventory.CraftItemStack");
        this.bukkitItemAsNmsCopyMethod = getMethodByReturnType(bukkitItemClass, nmsItemClass, null, ItemStack.class);

        this.nbtNumberClass = findClass("net.minecraft.nbt.NBTNumber",
                "net.minecraft.server." + nmsVersion + ".NBTNumber");
        this.nbtNumberAsNumberMethod = getMethodByReturnType(this.nbtNumberClass, Number.class, null);

        this.nbtStringClass = findClass("net.minecraft.nbt.NBTTagString",
                "net.minecraft.server." + nmsVersion + ".NBTTagString");
        this.nbtStringAsStringMethod = getMethodByReturnType(this.nbtStringClass, String.class,
                method -> !method.getName().equals("toString"));
    }

    public JsonElement convertItemToElement(ItemStack itemStack)
    {
        try
        {
            Object nmsItem = this.bukkitItemAsNmsCopyMethod.invoke(null, itemStack);

            Object itemTag = this.nbtCompoundConstructor.newInstance();
            this.saveItemMethod.invoke(nmsItem, itemTag);
            this.nbtCompoundRemoveMethod.invoke(itemTag, "Count");

            return this.nbtTagToElement(itemTag);
        }
        catch (IllegalAccessException | InvocationTargetException | InstantiationException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private JsonElement nbtTagToElement(Object tag) throws InvocationTargetException, IllegalAccessException
    {
        if (!this.nbtBaseClass.isInstance(tag))
        {
            throw new IllegalArgumentException("Tag is not NBTBase child");
        }
        if (this.nbtCompoundClass.isInstance(tag))
        {
            //noinspection unchecked
            Set<String> keys = (Set<String>) this.nbtCompoundGetKeysMethod.invoke(tag);

            JsonObject element = new JsonObject();

            for (String key : keys)
            {
                element.add(key, this.nbtTagToElement(this.nbtCompoundGetMethod.invoke(tag, key)));
            }
            return element;
        }
        else if (tag instanceof AbstractList)
        {
            AbstractList<?> list = (AbstractList<?>) tag;

            JsonArray element = new JsonArray();
            for (Object item : list)
            {
                element.add(this.nbtTagToElement(item));
            }
            return element;
        }
        else if (this.nbtNumberClass.isInstance(tag))
        {
            return new JsonPrimitive(new LazilyParsedNumber(String.valueOf(this.nbtNumberAsNumberMethod.invoke(tag))));
        }
        else if (this.nbtStringClass.isInstance(tag))
        {
            return new JsonPrimitive((String) this.nbtStringAsStringMethod.invoke(tag));
        }
        throw new IllegalStateException("Could not convert nbt tag '" + tag.getClass() + "'");
    }

    private static Class<?> findClass(String... classNames) throws ClassNotFoundException
    {
        for (String name : classNames)
        {
            try
            {
                return Class.forName(name);
            }
            catch (ClassNotFoundException ignored)
            {
            }
        }
        throw new ClassNotFoundException(String.join(", ", classNames));
    }

    private static Method getMethodByReturnType(Class<?> baseClass, Class<?> returnType, Predicate<Method> predicate,
            Class<?>... parameters)
    {
        for (Method method : baseClass.getDeclaredMethods())
        {
            if (method.getReturnType().equals(returnType) && (parameters == null || Arrays.equals(
                    method.getParameterTypes(), parameters)) && (predicate == null || predicate.test(method)))
            {
                return method;
            }
        }
        return null;
    }
}
