package pl.voxele.minecraft.creative;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

public class CreateUnsafeItems
        implements Listener
{
    private final Map<JsonElement, String> validItemsMap = new HashMap<>();
    private final Map<UUID, ItemStackTuple> pickedItemMap = new HashMap<>();
    private final Map<UUID, ItemStackTuple> similarItemMap = new HashMap<>();

    private final Logger logger;
    private final ReflectionHelper reflectionHelper;

    public CreateUnsafeItems(Logger logger, ReflectionHelper reflectionHelper, JsonObject items)
    {
        this.logger = logger;
        this.reflectionHelper = reflectionHelper;
        for (Map.Entry<String, JsonElement> entry : items.entrySet())
        {
            String group = entry.getKey();
            for (JsonElement item : entry.getValue().getAsJsonArray())
            {
                this.validItemsMap.put(item, group);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onCreateCreativeItem(InventoryClickEvent event)
    {
        HumanEntity entity = event.getWhoClicked();
        if (event.getAction() == InventoryAction.NOTHING)
        {
            return;
        }
        if (this.isPlace(event.getAction()) && event.getSlotType() == InventoryType.SlotType.RESULT)
        {
            return;
        }
        if (entity.getGameMode() != GameMode.CREATIVE)
        {
            return;
        }
        if (entity.hasPermission("safe-creative.bypass"))
        {
            return;
        }
        ItemStack targetItem = event.getCursor();
        if (targetItem == null)
        {
            return;
        }
        if (event.getClick() == ClickType.CREATIVE && targetItem.isSimilar(event.getCurrentItem()))
        {
            return;
        }
        UUID uniqueId = entity.getUniqueId();

        ItemStackTuple pickedItem = this.pickedItemMap.get(uniqueId);
        boolean itemPicked = pickedItem != null && pickedItem.equalsOrLessThanExpected(targetItem);
        if (targetItem.getType() == Material.AIR || itemPicked)
        {
            return;
        }
        JsonElement element = this.reflectionHelper.convertItemToElement(targetItem);
        String group = this.validItemsMap.get(element);

        ItemStackTuple similarItem = this.similarItemMap.get(uniqueId);
        boolean itemSimilar = similarItem != null && similarItem.equalsOrLessThanExpected(targetItem);
        if (group == null && !itemSimilar)
        {
            this.logger.info(entity.getName() + " attempt to create unsafe creative item: '" + element + "'");
        }
        if (group == null || !entity.hasPermission("safe-creative.group." + group))
        {
            event.setCursor(new ItemStack(Material.AIR));
            event.setCancelled(true);
            this.pickedItemMap.remove(entity.getUniqueId());
        }
        this.similarItemMap.put(uniqueId, new ItemStackTuple(targetItem));
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onCreateCreativeItem(InventoryDragEvent event)
    {
        HumanEntity entity = event.getWhoClicked();
        if (entity.hasPermission("safe-creative.bypass"))
        {
            return;
        }
        UUID uniqueId = entity.getUniqueId();

        ItemStack cursor = event.getCursor();
        ItemStack oldCursor = event.getOldCursor();

        int amount = 0;
        if (oldCursor != null && oldCursor.isSimilar(cursor))
        {
            amount += cursor.getAmount();
        }
        for (ItemStack targetItem : event.getNewItems().values())
        {
            if (targetItem.isSimilar(oldCursor))
            {
                amount += targetItem.getAmount();
            }
        }
        if (oldCursor != null && amount == oldCursor.getAmount())
        {
            return;
        }
        for (ItemStack targetItem : event.getNewItems().values())
        {
            if (targetItem.getType() == Material.AIR)
            {
                continue;
            }
            JsonElement element = this.reflectionHelper.convertItemToElement(targetItem);
            String group = this.validItemsMap.get(element);

            ItemStackTuple similarItem = this.similarItemMap.get(uniqueId);
            boolean itemSimilar = similarItem != null && similarItem.equalsOrLessThanExpected(targetItem);
            if (group == null && !itemSimilar)
            {
                this.logger.info(entity.getName() + " attempt to create unsafe creative item: '" + element + "'");
            }
            if (group == null || !entity.hasPermission("safe-creative.group." + group))
            {
                event.setCancelled(true);
                return;
            }
            this.similarItemMap.put(uniqueId, new ItemStackTuple(targetItem));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void monitorCreateCreativeItem(InventoryClickEvent event)
    {
        HumanEntity entity = event.getWhoClicked();
        if (event.getAction() == InventoryAction.NOTHING)
        {
            return;
        }
        if (this.isPlace(event.getAction()) && event.getSlotType() == InventoryType.SlotType.RESULT)
        {
            return;
        }
        this.pickedItemMap.remove(entity.getUniqueId());

        ItemStack currentItem = event.getCurrentItem();
        if (currentItem == null)
        {
            return;
        }
        if (entity.getGameMode() != GameMode.CREATIVE)
        {
            return;
        }
        if (event.getAction() == InventoryAction.CLONE_STACK)
        {
            return;
        }
        this.pickedItemMap.put(entity.getUniqueId(), new ItemStackTuple(currentItem));
    }

    private boolean isPlace(InventoryAction action)
    {
        return action == InventoryAction.PLACE_ONE || action == InventoryAction.PLACE_SOME || action == InventoryAction.PLACE_ALL;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        UUID uniqueId = event.getPlayer().getUniqueId();
        this.pickedItemMap.remove(uniqueId);
        this.similarItemMap.remove(uniqueId);
    }
}
