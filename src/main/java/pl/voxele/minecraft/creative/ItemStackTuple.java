package pl.voxele.minecraft.creative;

import org.bukkit.inventory.ItemStack;

import java.util.StringJoiner;

public class ItemStackTuple
{
    private final ItemStack original;
    private final ItemStack clone;

    public ItemStackTuple(ItemStack original)
    {
        this.original = original;
        this.clone = this.original.clone();
    }

    public boolean equalsOrLessThanExpected(ItemStack itemStack)
    {
        if (this.original.isSimilar(itemStack) && itemStack.getAmount() <= this.original.getAmount())
        {
            return true;
        }
        return this.clone.isSimilar(itemStack) && itemStack.getAmount() <= this.clone.getAmount();
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", ItemStackTuple.class.getSimpleName() + "[", "]").add("original=" + original)
                .add("clone=" + clone)
                .toString();
    }
}
