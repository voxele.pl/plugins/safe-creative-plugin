package pl.voxele.minecraft.creative.worldedit;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.event.extent.EditSessionEvent;
import com.sk89q.worldedit.extension.platform.Actor;
import com.sk89q.worldedit.extent.NullExtent;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.SessionManager;
import com.sk89q.worldedit.util.eventbus.Subscribe;
import com.sk89q.worldedit.world.World;
import org.bukkit.Bukkit;
import org.bukkit.WorldBorder;

import java.util.logging.Level;

public class AntiWorldEditCrash
{
    @Subscribe
    public void onEditSession(EditSessionEvent event)
    {
        Actor actor = event.getActor();
        if (actor == null)
        {
            return;
        }

        World world = event.getWorld();
        if (world == null)
        {
            return;
        }
        org.bukkit.World bukkitWorld = Bukkit.getWorld(world.getName());
        if (bukkitWorld == null)
        {
            return;
        }
        WorldBorder border = bukkitWorld.getWorldBorder();
        double minX = border.getCenter().getX() - border.getSize() / 2;
        double minZ = border.getCenter().getZ() - border.getSize() / 2;
        double maxX = border.getCenter().getX() + border.getSize() / 2;
        double maxZ = border.getCenter().getZ() + border.getSize() / 2;

        SessionManager manager = WorldEdit.getInstance().getSessionManager();
        LocalSession localSession = manager.get(actor);
        Region selection;
        try
        {
            selection = localSession.getSelection(world);
        }
        catch (IncompleteRegionException e)
        {
            Bukkit.getLogger().log(Level.SEVERE, "Can not get player selection", e);
            return;
        }
        BlockVector3 minimumPoint = selection.getMinimumPoint();
        BlockVector3 maximumPoint = selection.getMaximumPoint();

        boolean cancel = false;
        cancel |= maximumPoint.getX() > maxX;
        cancel |= maximumPoint.getX() < minX;
        cancel |= minimumPoint.getX() > maxX;
        cancel |= minimumPoint.getX() < minX;

        cancel |= maximumPoint.getZ() > maxZ;
        cancel |= maximumPoint.getZ() < minZ;
        cancel |= minimumPoint.getZ() > maxZ;
        cancel |= minimumPoint.getZ() < minZ;

        if (cancel)
        {
            event.setExtent(new NullExtent());
            event.getTracingExtents().clear();
            Bukkit.getLogger().warning(actor.getName() + " reach maximum or minimum point of map, cancel edit");
        }
    }
}
